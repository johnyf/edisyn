import pdb
import sys

def generate_plant(m, n):
    """generate the mxn gridworld plant with Alice
    grid is labeled from 0 to mxn-1"""
    fout = open ('grid{}{}.fsm'.format(m,n), 'w')
    state_table = dict()#dict({'init': dict()})
    event_set = set()
    secret = set()
    for i in range(m*n):
        if i == 0:
            secret.add(0)
        state_table.update({i : dict()})
        state_table[i].update({ 'a{i}{i}'.format(i = i): i})
        event_set.add('a{i}{i}'.format(i = i))

        neighbors = list()
        if i%n != n-1:
            neighbors.append(i+1)
        if i%n != 0:
            neighbors.append(i-1)
        if i+n < m*n:
            neighbors.append(i+n)
        if i-n >= 0:
            neighbors.append(i-n)
        
        for nei in neighbors:
            ev = 'a{i}{ii}'.format(i = i, ii = nei)
            event_set.add(ev)
            state_table[i].update({ ev : (nei)})
    print event_set
    fout.write(str(len(state_table)+1) + '\t' + str(len(event_set)) + '\n')
    fout.write('\ninit\t1\t' + str(n*m) + '\n')
    for i in range(n*m):
        fout.write('a{i}{i}\t{i}'.format(i = i) + '\n')    
    for s in state_table:
        if s in secret:
            fout.write('\n{i}'.format(i = s) + '\t0\t' + str(len(state_table[s])) + '\n')
        else:
            fout.write('\n{i}'.format(i = s) + '\t1\t' + str(len(state_table[s])) + '\n')
        for t in state_table[s]:
            fout.write(t + '\t{i}'.format(i = state_table[s][t]) + '\n')
    fout.close()
    print 'done constructing grid{}{}.fsm'.format(m,n)
    return state_table

def line_of_sight_utility(trans, m, n):
    """line of sight utility: accepted if on the same column or row"""
    fout = open ('util{}{}.spc'.format(m, n), 'w')
    """for each pair in trans, output if line of sight holds"""
    fout.write('init\tinit\n')
    for s in trans:
        for ns in trans:
            if (s % n == ns % n) or (s/n == ns/n):
                fout.write('{s}\t{ns}\n'.format(s = s, ns = ns))
    fout.close()
    print 'done constructing util{}{}.spc'.format(m, n)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print "Usage: python case_generator.py <x-size> <y-size>"
        sys.exit(0)
    m = int(sys.argv[1])
    n = int(sys.argv[2])
    trans = generate_plant(m,n)
    line_of_sight_utility(trans, m, n)
