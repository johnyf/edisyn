import sh
import re
import matplotlib.pyplot as plt 
k = 2
fout = open('result.txt','w')
fout.seek(0)
sh.cd('../../')
for m in range(2, 4):
    fout.write('m={m}\n'.format(m=m))
    n = m
    print "secretLocation {m} {n}".format(m = m, n = n)
    sh.cd('examples/secretLocation/')
    sh.python("case_generator.py", m, n)
    ns, nev = sh.head("-n", "1", "grid{m}{n}.fsm".format(m = m, n = n)).strip().split()
    x = int(ns) + int(nev)
    plant="../examples/secretLocation/grid{m}{n}.fsm".format(m = m, n = n)
    utility="../examples/secretLocation/util{m}{n}.spc".format(m = m, n = n)
    obf="../examples/secretLocation/obf{m}{n}.fsm".format(m = m, n = n)
    sh.cd('../../edisyn/')
    out = sh.python("edisyn.py", plant, utility, obf)
    fout.write('num_variables: {x}\n {out}\n'.format(n = n, x = x, out = out))
    print "num_variables", x
    print out 
    sh.cd('../')
