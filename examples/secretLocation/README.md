This folder contains secretLocation examples constructed for scalability test.
Specifically, gridmn is the example where the Alice moves on the mxn grid world. 
The secret location here is the top-left corner in the grid world.
gridmn.fsm is the finite-state automaton model representing the movements of Alice.
Each state is the index of Alice's location.
Locations are indices from 0 to mn-1 starting from the top-left cell, from left to right, top to bottom.
The only secret state here is state 0.
Only movements to neighboring locations are allowed.
Each event represents Alice's one-step movement. 
For example, a12 means Alice moves from cell 1 to cell 2.
Finally an auxiliary initial state "init" is introduced; it represents a state where Alice's location is unknown.
There is a transition from init to every state i with event label aii. 



1) .fsm is a format representing the finite-state automaton.
It starts from a line of two numbers: the number of state (n), the number of events (m)
Then, there are n number of blocks. Each block describes the transitions from a given state.
In each block, the first line contains: the state name, the marking (0 or 1), and the number of outgoing transitions (k)
It then followed by k lines, where each line contains: the event label, and the name of the next state.

2) .spc if a format representing the utility specification.
It contains a set of state pairs for which the utility loss is bounded by the budget.
Here, we set the utility budget to be line of sight. That is, the utility loss in mapping from state i to state j is tolerable if they are on the same column or row.

3) obf.fsm is the format representing the obfuscator.

4) case\_generator.py generates the example of gridmn with line-of-sight utility. 
```python case\_generator.py m n``` generates files gridmn.fsm and utilmn.spc

5) scalability.py performs the scalability experiment for the grid world examples with increasing sizes. It runs EdiSyn with mxm grid world starting from m=2, and reports the synthesis time and memory usage.
