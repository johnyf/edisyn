import pdb
import sys

def generate_plant(m, n):
    """generate the mxn gridworld plant with 2 people
    grid is labeled from 0 to mxn-1"""
    fout = open ('grid{}{}.fsm'.format(m,n), 'w')
    state_table = dict()#dict({'init': dict()})
    event_set = set()
    secret = set()
    for i in range(m*n):
        for j in range(m*n):
            if i == j:
                secret.add((i, j))
            state_table.update({(i, j) : dict()})
            state_table[(i,j)].update({ 'a{i}{i}b{j}{j}'.format(i = i, j = j): (i,j)})

            neighbors = list()
            nei_i = [i] 
            nei_j = [j]
            if i%n != n-1:
                nei_i.append(i+1)
            if i%n != 0:
                nei_i.append(i-1)
            if i+n < m*n:
                nei_i.append(i+n)
            if i-n >= 0:
                nei_i.append(i-n)
            if j%n != n-1:
                nei_j.append(j+1)
            if j%n != 0:
                nei_j.append(j-1)
            if j+n < m*n:
                nei_j.append(j+n)
            if j-n >= 0:
                nei_j.append(j-n)
            
            for ni in nei_i:
                for nj in nei_j:
                    neighbors.append((ni,nj))
            for nei in neighbors:
                ev = 'a{i}{ii}b{j}{jj}'.format(i = i, ii = nei[0], j = j, jj = nei[1])
                event_set.add(ev)
                state_table[(i,j)].update({ ev : (nei[0], nei[1])})
    fout.write(str(len(state_table)+1) + '\t' + str(len(event_set)) + '\n')
    fout.write('\ninit\t1\t' + str(pow(n*m,2)) + '\n')
    for i in range(n*m):
        for j in range(n*m):
            fout.write('a{i}{i}b{j}{j}\t{i},{j}'.format(i = i, j = j) + '\n')    
    for s in state_table:
        if s in secret:
            fout.write('\n{i},{j}'.format(i = s[0], j = s[1]) + '\t0\t' + str(len(state_table[s])) + '\n')
        else:
            fout.write('\n{i},{j}'.format(i = s[0], j = s[1]) + '\t1\t' + str(len(state_table[s])) + '\n')
        for t in state_table[s]:
            fout.write(t + '\t{i},{j}'.format(i = state_table[s][t][0], j = state_table[s][t][1]) + '\n')
    fout.close()
    print 'done constructing grid{}{}.fsm'.format(m,n)
    return state_table

def generate_utility(trans, dmax, m, n):
    """dmax is the tolerable L1-distance"""
    fout = open ('util{}{}{}.spc'.format(m, n, dmax), 'w')
    """for each pair in trans, output if norm is less or equal to d"""
    fout.write('init\tinit\n')
    for s in trans:
        for ns in trans:
            s0x = s[0]%n
            s0y = s[0]/n
            s1x = s[1]%n
            s1y = s[1]/n
            ns0x = ns[0]%n
            ns0y = ns[0]/n
            ns1x = ns[1]%n
            ns1y = ns[1]/n
            d = abs(s0x-ns0x)+abs(s0y-ns0y)+abs(s1x-ns1x)+abs(s1y-ns1y)
            if (d <= dmax):
                fout.write('{i},{j}'.format(i = s[0], j = s[1]) + '\t{i},{j}\n'.format(i = ns[0], j = ns[1]))
#                print'{i},{j}'.format(i = s[0], j = s[1]) + '\t{i},{j}'.format(i = ns[0], j = ns[1])
#                print '({ix},{iy}),({jx},{jy})'.format(ix = s0x, jx = s1x, iy = s0y, jy = s1y) + '\t({ix},{iy}),({jx},{jy})\n'.format(ix = ns0x, jx = ns1x, iy = ns0y, jy = ns1y)
    fout.close()
    print 'done constructing util{}{}{}.spc'.format(m, n, dmax)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print "Usage: python dexperiment.py <x-size> <y-size> <max_tolerable_distance>"
        sys.exit(0)
    m = int(sys.argv[1])
    n = int(sys.argv[2])
    kmax = int(sys.argv[3])
    trans = generate_plant(m,n)
    generate_utility(trans, kmax, m, n)
