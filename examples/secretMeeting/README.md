This folder contains secretMeeting examples constructed for scalability test.
Specifically, gridmn is the example where the Alice and Bob move on the mxn grid world. 
The secret here is that Alice and Bob meet; that is, they are in the same cell of the grid.
gridmn.fsm is the finite-state automaton model representing the movements of Alice and Bob.
Each state is a tuple (la,lb) where la is the location of Alice and lb is the location of Bob.
Locations are indices from 0 to mn-1 starting from the top-left cell, from left to right, top to bottom.
Only movements to neighboring locations are allowed.
Each event represents Alice and Bob's one-step movement. 
For example a12b03 means Alice moves from cell 1 to cell 2, and Bob moves from cell 0 to cell 3.
Finally an auxiliary initial state "init" is introduced.
There is a transition from init to every state (i,j) with event label aiibjj. 


1) .fsm is a format representing the finite-state automaton.
It starts from a line of two numbers: the number of state (n), the number of events (m)
Then, there are n number of blocks. Each block describes the transitions from a given state.
In each block, the first line contains: the state name, the marking (0 or 1), and the number of outgoing transitions (k)
It then followed by k lines, where each line contains: the event label, and the name of the next state.

2) .spc if a format representing the utility specification.
It contains a set of state pairs for which the utility loss is bounded by the budget.
Here, we set the utility budget to be the L1 norm

3) obf.fsm is the format representing the obfuscator.

4) case_generator.py generates the example of gridmn with utility budget k. 
```python case_generator.py m n k``` generates files gridmn.fsm and utilmnk.spc

5) scalability.py performs the scalability experiment for the grid world examples with increasing sizes. It runs EdiSyn with mxm grid world starting from m=2, and reports the synthesis time and memory usage.
