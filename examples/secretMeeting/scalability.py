import sh
import re
import matplotlib.pyplot as plt 
k = 2
fout = open('result.txt','w')
fout.seek(0)
sh.cd('../../')
for m in range(2, 5):
    fout.write('m={m}\n'.format(m=m))
    #for n in range(2,3):
    n = m
    print "secretMeeting {m} {n} {k}".format(m = m, n = n, k = 2)
    sh.cd('examples/secretMeeting/')
    sh.python("case_generator.py", m, n, k)
    ns, nev = sh.head("-n", "1", "grid{m}{n}.fsm".format(m = m, n = n)).strip().split()
    x = int(ns) + int(nev)
    plant="../examples/secretMeeting/grid{m}{n}.fsm".format(m = m, n = n)
    utility="../examples/secretMeeting/util{m}{n}{k}.spc".format(m = m, n = n, k = 2)
    obf="../examples/secretMeeting/obf{m}{n}{k}.fsm".format(m = m, n = n, k = 2)
    sh.cd('../../edisyn/')
    out = sh.python("edisyn.py", plant, utility, obf)
    fout.write('num_variables: {x}\n {out}\n'.format(n = n, x = x, out = out))
    print "num_variables", x
    print out 
    sh.cd('../')
