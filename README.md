EdiSyn is a Python-based toolkit for synthesizing obfuscators that enforces privacy while preserving utility with formal guarantees.

EdiSyn was developed by Yi-Chin Wu while she was a postdoc with the University of Michigan, Ann Arbor, and the University of California, Berkeley. It is distributed under BSD 3-Clause license. As of Dec. 2016, this repository is no longer updated. The current version of EdiSyn can be found at https://gitlab.eecs.umich.edu/M-DES-tools.


## Installation
1. Install EdiSyn  
    ```cd <folder_to_install_EdiSyn>```  
    ```git clone https://yichinwu@bitbucket.org/yichinwu/edisyn.git```  
    ```cd edisyn```
2. Install [dd](https://github.com/johnyf/dd), a Python package for BDDs and MDDs. It is sufficient to install the CUDD library wrapper by the following instruction:   
    Install Cython: ```pip install cython```  
    Clone dd: ```git clone https://github.com/johnyf/dd.git```   
    Use the customized ```cudd.pyx``` from EdiSyn: ```cp cudd.pyx dd/dd/cudd.pyx```   
    Install dd: ```cd dd```   
    ```python setup.py install --fetch --cudd```     
4. EdiSyn is ready to use

## Run EdiSyn
1. Go back to the folder where you installed EdiSyn
2. ```cd edisyn```
3. Synthesze an obfuscator from the plant and the utility files: 
    * Standard synthesis: ```python editSyn.py <plant_fsm_file> <utility_file> <obfuscator_file>```
    * Synthesis with bounded insertion length k: ```python editSyn.py -b <k> <plant_fsm_file> <utility_file> <output_file>```
    * Synthesis of nondeterministic edit strategies: ```python editsyn.py -n <plant_fsm_file> <utility_file> <output_file>```
4. Once an obfuscator is synthesized, we can simulate it by: 
    * Random simulation of k steps: ```python simulation.py -r <k> <obfuscator_file>```
    * Interactive simulation: ```python simulation.py -i <obfuscator_file>```

## Tests
Require [nose](https://nose.readthedocs.org/en/latest/). Run with:
```cd edisyn``` ```nosetests```

## Contact Us
Questions? Send email to yichin.wu[at]berkeley[dot]edu

## Acknowledgments
This work was supported in part by the NSF Expeditions in Computing project ExCAPE: Expeditions in Computer Augmented Program Engineering (grant CCF-1138860), and in part by the TerraSwarm Research Center, one of six centers supported by the STARnet phase of the Focus Cen- ter Research Program (FCRP) a Semiconductor Research Corporation program sponsored by MARCO and DARPA.
