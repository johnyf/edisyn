# the way to use bdd is that each BDD() is a manager, managing a specific var order
# a manager can mangage several functions, each function knows what manager it belongs to  
from dd import cudd
import copy
import natsort
import bitvector as bv
import pdb

class Automaton(object):

    def __init__(self):
        # vars
        self.vars = dict()
        # map between primed and unprimed
        self.prime = dict() 
        self.unprime = dict()
        # aux
        self.bdd = cudd.BDD()
        # formulae, bdd formula for trans and secret
        self.trans = self.bdd.False
        self.insert_trans = self.bdd.False
        self.replace_trans = self.bdd.False
        self.secret= self.bdd.False
        self.init = self.bdd.False

    def __str__(self):
        c = ['symbolic fsm:', 'vars = {v}', 'init = {init}','trans = {trans}', 'insert_trans = {itrans}', 'replace_trans = {rtrans}', 'secret = {secret}']
        return '\n'.join(c).format(v = self.vars['state']['bitnames']+self.vars['event']['bitnames']+self.vars['nstate']['bitnames'], init = self.init, trans = self.trans, itrans = self.insert_trans, rtrans = self.replace_trans, secret = self.secret)
            
    def __copy__(self):
        a = Automaton()
        a.vars = copy.deepcopy(self.vars)
        cudd.copy_vars(self.bdd, a.bdd)
        a.trans = cudd.copy_bdd(self.trans, self.bdd, a.bdd)
        a.insert_trans = cudd.copy_bdd(self.insert_trans, self.bdd, a.bdd)
        a.replace_trans = cudd.copy_bdd(self.replace_trans, self.bdd, a.bdd)
        a.secret = cudd.copy_bdd(self.secret, self.bdd, a.bdd)
        a.init = cudd.copy_bdd(self.init, self.bdd, a.bdd)
        
        a.prime = copy.copy(self.prime)
        a.unprime = copy.copy(self.unprime)
        return a    
   
    def copy_rename_states(self, newname = None):
        assert newname is not None
        #deep copy
        a = Automaton()
        a = copy.copy(self)
        assert a.bdd.vars == self.bdd.vars
        #name change mapping
        nchange = dict()
        for i, bname in enumerate(a.vars['state']['bitnames']):
            nchange.update({bname: newname+"_{i}".format(i = i)})
            a.bdd.add_var(newname+"_{i}".format(i = i))
        for i, bname in enumerate(a.vars['nstate']['bitnames']):
            nchange.update({bname: "n"+newname+"_{i}".format(i = i)})
            a.bdd.add_var('n'+newname+"_{i}".format(i = i))
        #change names of trans
        a.trans = cudd.rename(a.trans, a.bdd, nchange)
        #change names of secret
        a.secret = cudd.rename(a.secret, a.bdd, nchange)
        #change init name
        a.init = cudd.rename(a.init, a.bdd, nchange)
        #change names of vars
        a._change_name(newname)
        #change names of prime,unprime
        prime = dict()
        unprime = dict()
        for b in a.vars['state']['bitnames']:
            prime.update({b:'n'+b})
            unprime.update({'n'+b: b})
        a.prime = prime
        a.unprime = unprime
        return a
 
    def __del__(self):
        del self.init
        del self.trans
        del self.insert_trans
        del self.replace_trans
        del self.secret    


    def build(self, num_states, num_events):
        self.add_states(num_states)
        self.add_events(num_events)
        self.vars = bv.bitblast_table(self.vars)
        #decide order
        _bitvector_to_bdd(self)
        #TODO: how to handle multiple init?
        self.init = bv.var_to_cube(0, self.vars['state'], self.bdd)
        #construct maps between prime and unprime
        for bname in self.vars['state']['bitnames']:
            self.prime.update({bname:'n'+bname})
            self.unprime.update({'n'+bname: bname})

    def add_states(self, num_states):
        self.vars.update({'state': dict(name = 'r', dom = num_states)})
        self.vars.update({'nstate': dict(name = 'nr', dom = num_states)})


    #reserve 0 for the explicit "epsilon" event
    def add_events(self, num_events):
        self.vars.update({'event': dict(name = 'e', dom = num_events+1)})

    def add_trans(self, trans):
        for ev, sns in trans.iteritems():
            assert ev >= 0, ev
            assert ev <= self.vars['event']['dom'], ev
            for  s, nslist in sns.iteritems():
                assert s >= 0, s
                assert s < self.vars['state']['dom'], s
                #pdb.set_trace()
                for ns in nslist:
                    assert ns >= 0, ns
                    assert ns < self.vars['state']['dom'], ns
                    u = bv.tran_to_cube(ev, s, ns, self.vars, self.bdd)
                    self.trans = self.bdd.apply('or', self.trans, u)
                    del u


    """deprecated after the symbolic implementation of editFSM.
    See add_trans(self,trans) for the new version that only adds regular transitions
    def add_trans(self, trans, edit = None):
        for ev, sns in trans.iteritems():
            assert ev >= 0, ev 
            assert ev <= self.vars['event']['dom'], ev
            for  s, nslist in sns.iteritems():
                assert s >= 0, s
                assert s < self.vars['state']['dom'], s
                #pdb.set_trace()
                for ns in nslist:
                    assert ns >= 0, ns
                    assert ns < self.vars['state']['dom'], ns
                    u = bv.tran_to_cube(ev, s, ns, self.vars, self.bdd)
                    if self.bdd.cofactor_function(self.trans, u) == self.bdd.False: #new tran
                        self.trans = self.bdd.apply('or', self.trans, u)
                        if edit == 'i':
                            self.insert_trans = self.insert_trans | u
                        elif edit == 'r':
                            self.replace_trans = self.replace_trans | u
                    del u
    """

    def add_secret(self, secret):
        for s in secret:
            assert s >= 0, s
            assert s < self.vars['state']['dom'], s
            u = bv.var_to_cube(s, self.vars['state'], self.bdd)
            self.secret = self.bdd.apply('or', self.secret, u)
      
    def remove_secret_states(self):
        self.trans = self.trans & ~self.secret
        self.init = self.init & ~self.secret
        prime_secret = cudd.rename(self.secret, self.bdd, self.prime)       
        self.trans = self.trans & ~prime_secret


    def _change_name(self, newname = None):
        assert newname is not None
        nchange = dict()
        nbitnames = list()
        for i, bname in enumerate(self.vars['state']['bitnames']):
            nbname = '{s}_{i}'.format(s = newname, i = i)
            nbitnames.append(nbname)
            nchange.update({bname: nbname})
        self.vars['state']['bitnames'] = nbitnames
        self.vars['state']['name'] = newname
        nbitnames = list()
        for i, bname in enumerate(self.vars['nstate']['bitnames']):
            nbname = 'n{s}_{i}'.format(s = newname, i = i)
            nbitnames.append(nbname)
            nchange.update({bname:nbname})
        self.vars['nstate']['bitnames'] = nbitnames
        self.vars['nstate']['name'] = 'n'+newname
        return nchange

    def contains_tran(self, e, s, ns, edit = None):
        u = bv.tran_to_cube(e,s,ns,self.vars, self.bdd)
        if edit == 'i':
            return self.bdd.cofactor_function(self.insert_trans, u) == self.bdd.True
        elif edit == 'r':
            return self.bdd.cofactor_function(self.replace_trans, u) == self.bdd.True
        elif edit == None:
            return self.bdd.cofactor_function(self.trans, u) == self.bdd.True


class MealyAutomaton(object):
    def __init__(self):
        # vars
        self.vars = dict()
        self.prime = dict()
        self.unprime = dict()
      
        # formulae, bdd formula for trans and secret
        self.trans = None
        self.output_trans = None
        self.init = None
        self.bdd = None

    def __str__(self):
        c = ['Mealy automaton:', 'vars = {v}', 'init = {init}','trans = {trans}', 'out_trans = {out_trans}']
        return '\n'.join(c).format(v = self.vars['state']['bitnames']+self.vars['event']['bitnames']+self.vars['nstate']['bitnames'], init = self.init, trans = self.trans, out_trans = self.output_trans)

    def build(self,  game = None):
        if game != None:
            self.bdd = game.bdd
        else: 
            self.bdd = cudd.BDD()
        self.vars = game.vars
        
        self.trans = self.bdd.False
        self.output_trans = self.bdd.False
        self.init = game.init 

    def __del__(self):
        del self.init
        del self.trans
        del self.output_trans
        

def _bitvector_to_bdd(aut):
    _vars = aut.vars
    ordering = dict()
    ebits = bv.list_bits(_vars['event'])
    sbits = bv.list_bits(_vars['state'])
    spbits = bv.list_bits(_vars['nstate'])
    ordered_ebits = natsort.natsorted(ebits) 
    ordered_sbits = natsort.natsorted(sbits) 
    ordered_spbits = natsort.natsorted(spbits) 
    obits = ordered_ebits+ordered_sbits+ordered_spbits
    for level, bvar in enumerate(obits):
        aut.bdd.add_var(bvar,level)
        ordering[bvar]=level


if __name__ == '__main__':
    #note that 0 is reserved for epsilon
    
    trans = {1: {1:[1], 0:[2]}} #{event:{state:[nstate]}}
    a = Automaton()
    a.build(3,2,2)
    a.add_trans(trans)
    a.add_secret({1})
    assert a.contains_tran(1,1,1) == True and a.contains_tran(1,0,0)== True
    a.remove_secret_states()
    assert a.contains_tran(1,1,1) == False and a.contains_tran(1,0,0) == True
    #a.add_secret({1,0})
    #the composit fsm does not have sname, ename, this is the reason why build() fails
    #shold out build() depending on sname, ename
    #note that here, the two fsa have the same event sets, we can simply take the conjunction of the transitions
