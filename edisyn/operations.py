from dd import cudd
import copy
import automaton as at
import bitvector as bv
import pdb
import time
import settings
import synthesis as syn

def build_good_editFSM(aut, trans, util):
    a1 = build_editFSM(aut, trans)
    a2 = build_publicFSM(aut)
    #if a2 is not empty
    if a2.init == a2.bdd.False:
        return None
    return _build_good_editFSM(a1, a2, util)
 

#[TODO] dom not updated, assume not important for now
def _build_good_editFSM(edit, public, util):
    """edit and public are constructed from the same plant fsm.
    Hence, there is only one single event set
    """
    assert public.init != public.bdd.False 
#    assert edit.vars['event']['name'] == public.vars['event']['name']
#    assert edit.vars['event']['width'] == public.vars['event']['width'] 
    a = at.Automaton()
    
    #vars
    a.vars['state'] = merge_vardata(edit.vars['state'], public.vars['state'])
    a.vars['nstate'] = merge_vardata(edit.vars['nstate'], public.vars['nstate'])
    a.vars['event'] = edit.vars['event']#event set = event set of editfsm
    cudd.copy_vars(public.bdd, a.bdd)
    #prime,unprime
    for b in a.vars['state']['bitnames']:
        a.prime.update({b:'n'+b})
        a.unprime.update({'n'+b: b})

    #init
    edit_init = cudd.copy_bdd(edit.init, edit.bdd, a.bdd)
    public_init = cudd.copy_bdd(public.init, public.bdd, a.bdd)
    a.init = edit_init & public_init

    #trans
    edit_trans = cudd.copy_bdd(edit.trans, edit.bdd, a.bdd)
    edit_insert_trans = cudd.copy_bdd(edit.insert_trans, edit.bdd, a.bdd)
    edit_replace_trans = cudd.copy_bdd(edit.replace_trans, edit.bdd, a.bdd)
    public_trans = cudd.copy_bdd(public.trans, public.bdd, a.bdd)
    #restrict to valid util pairs
    if util is None:
        good_pair = a.bdd.True 
    else:
        good_pair = a.bdd.False
        for pair in util:
            r = bv.var_to_cube(pair[0], edit.vars['state'], a.bdd)           
            f = bv.var_to_cube(pair[1], public.vars['state'], a.bdd)           
            good_pair = good_pair | r & f
    n_good_pair = cudd.rename(good_pair, a.bdd, a.prime)
    a.trans = edit_trans & public_trans & good_pair & n_good_pair
    a.insert_trans = edit_insert_trans & public_trans & good_pair & n_good_pair
    a.replace_trans = edit_replace_trans & public_trans & good_pair & n_good_pair
    f = a.insert_trans
    return a 


def merge_vardata(vardata1, vardata2):
    assert vardata1['name'] != vardata2['name']
    vardata = dict()
    vardata['name'] = '({x},{y})'.format(x = vardata1['name'], y = vardata2['name'])
    vardata['width'] = vardata1['width']+ vardata2['width']
    vardata['bitnames'] = vardata1['bitnames'] + vardata2['bitnames']
    return vardata

def build_editFSM(aut, trans_dict):
    """This is constructed directly from plant. Hence, we destroy the original plant.
    """
    aut.replace_trans = aut.bdd.quantify(aut.trans, aut.vars['event']['bitnames'], False) & ~aut.trans
    insert_trans = aut.bdd.True
    for sbit in aut.vars['state']['bitnames']:
        f = aut.bdd.add_expr('{q} & n{q}'.format(q=sbit)) | aut.bdd.add_expr('!{q} & !n{q}'.format(q=sbit))
        insert_trans = insert_trans & f
    for s in xrange (aut.vars['state']['dom']):
        insert_trans = insert_trans & ~bv.tran_to_cube(0, s, s, aut.vars, aut.bdd)
    aut.insert_trans = insert_trans & ~aut.trans
    #aut.bdd.print_minterm(insert_trans)
    return aut


def build_publicFSM(aut):
    """The publicFSM is constructed by copying the original aut (with new names)
    and then remove secret states. Copying is needed because of the new names"""
    a = aut.copy_rename_states('f')
    #add_epsilon_sl(a)    
    for s in xrange (aut.vars['state']['dom']):
        a.trans = a.trans | bv.tran_to_cube(0, s, s, a.vars, a.bdd) 
    a.remove_secret_states()
    #a.bdd.print_minterm(a.trans)
    return a

def add_insert_alias(aut):
    """deprecated, inserts are now implemented symbolically"""
    insert_trans = dict()
    for e in xrange(1, aut.vars['event']['dom']):
        insert_trans.update({e:dict()})
        for s in xrange (aut.vars['state']['dom']):
            insert_trans[e].update({s:[s]})
    aut.add_trans(insert_trans, 'i')

def add_replace_alias(aut, trans):
    """0 is reserved for epsilon event
    deprecated, as replacements are now implemented symbolically"""
    replace_trans = dict()
    for ev in xrange(aut.vars['event']['dom']):
        replace_trans[ev]= dict()
    for e, sns in trans.iteritems():
        for s, nslist in sns.iteritems():
            for ns in nslist:
                for ev in xrange(aut.vars['event']['dom']):
                    if ev != e:
                        if s not in replace_trans[ev].keys():
                            replace_trans[ev].update({s:list()})
                        replace_trans[ev][s].append(ns)
    aut.add_trans(replace_trans,'r')

def add_epsilon_sl(aut):
    """deprecated, self-loops are now implemented symbolically"""
    eptrans = {0: dict()}
    for s in xrange (aut.vars['state']['dom']):
        eptrans[0].update({s:[s]})
    
    aut.add_trans(eptrans)

def test_edit(aut, trans):
    """deprecated, moved to test.py"""
    for e, sns in trans.iteritems():
        for s, nslist in sns.iteritems():
            for ns in nslist:
                assert aut.contains_tran(e, s, ns) == True
                for ev in xrange(aut.vars['event']['dom']):
                    if ev != e:
                        assert aut.contains_tran(ev, s, ns, 'r')
    for ev in xrange(1,aut.vars['event']['dom']):
        for s in xrange(aut.vars['state']['dom']):
            #pdb.set_trace()
            if ev not in trans.keys() or s not in trans[ev].keys() or s not in trans[ev][s]:
                assert aut.contains_tran(ev, s, s, 'i')

def test_public(aut, trans, secret):
    """deprecated, moved to test.py"""
    for e, sns in trans.iteritems():
        for s, nslist in trans[e].iteritems():
            for ns in nslist:
                if s in secret or ns in secret:
                    assert aut.contains_tran(e,s,ns) == False
                else:
                    assert aut.contains_tran(e,s,ns) == True    
    for s in xrange(aut.vars['state']['dom']):              
        if s not in secret and ns not in secret:
            assert aut.contains_tran(0,s,s) == True


if __name__ == '__main__':
    trans = {1: {1:[1], 0:[2]}}
    secret = {1}
    a = at.Automaton()
    a.build(3,1)
    a.add_trans(trans)
    a.add_secret(secret)
    a1 = build_editFSM(a, trans)
    test_edit(a1,trans)
    a2 = build_publicFSM(a)
    test_public(a2, trans, secret)
    
