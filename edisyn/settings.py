import time

def init():
    global start_time
    start_time = time.time()

def get_start_time():
    return start_time

