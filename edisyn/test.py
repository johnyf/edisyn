from dd import cudd
import automaton as at
import nose.tools as nt
import bitvector as bv
import pdb
import sys
import operations as op
import edisyn

def test():
    bdd = cudd.BDD()
    bdd.add_var('x')
    bdd.add_var('y')
    x = bdd.var('x')
    y = bdd.var('y')
    var = dict({'bitnames': list()})
    var['bitnames'].append('x')
    var['bitnames'].append('y')
    f1 = bdd.add_expr('!x&!y')
    u = bdd.cofactor(f1, dict(x=True))
    assert u == bdd.False
    del f1,x,y,u


def test_trans():
    a, trans, secret = build_aut1()
    for e, sns in trans.iteritems():
        for s, nslist in sns.iteritems():
            for ns in nslist:
                assert a.contains_tran(e,s,ns) == True 
    

def build_aut0():
    """num_states = 3
    num_events = 1
    trans = {1: {1:[1], 0:[2]}}
    secrets = {1}"""
    num_states = 3
    num_events = 2
    trans = {1: {0:[1]}, 2: {0:[2]}}
    secrets = {0}
    a = at.Automaton()
    a.build(num_states, num_events)
    a.add_trans(trans)
    a.add_secret(secrets)
    return a, trans, secrets

    
def build_aut1():
    """num_states = 3
    num_events = 1
    trans = {1: {1:[1], 0:[2]}}
    secrets = {1}"""
    num_states = 10
    num_events = 4
    trans = {1: {1:[2], 0:[5]}, 2: {2:[3], 5:[6], 0:[8]}, 3: {3:[4], 6:[7], 8:[9]}, 4:{0:[1]}}
    secrets = {7,9}
    a = at.Automaton()
    a.build(num_states, num_events)
    a.add_trans(trans)
    a.add_secret(secrets)
    return a, trans, secrets

def build_aut2():
    plant_file = "../examples/test1/plant.fsm"
    plant, trans, state_to_int, event_to_int, int_to_state, int_to_event = edisyn.fsm_to_aut(plant_file)
    return plant, trans 

def test_fsm2aut():
    plant_file = "../examples/test1/plant.fsm"
    plant, trans, state_to_int, event_to_int, int_to_state, int_to_event = edisyn.fsm_to_aut(plant_file)
    assert len(trans) == len(event_to_int)-1 and len(trans) == len(int_to_event)-1

def test_Gedit():
    a, trans, secret = build_aut1()
    g_edit = op.build_editFSM(a, trans)
    for e, sns in trans.iteritems():
        for s, nslist in sns.iteritems():
            for ns in nslist:
                assert g_edit.contains_tran(e, s, ns) == True
                for ev in xrange(g_edit.vars['event']['dom']):
                    if ev != e:
                        assert g_edit.contains_tran(ev, s, ns, 'r')
    for ev in xrange(1,g_edit.vars['event']['dom']):
        for s in xrange(g_edit.vars['state']['dom']):
            if ev not in trans.keys() or s not in trans[ev].keys() or s not in trans[ev][s]:
                assert g_edit.contains_tran(ev, s, s, 'i') 
    

def test_Gpublic():
    a, trans, secret = build_aut1()
    g_public = op.build_publicFSM(a)
    for e, sns in trans.iteritems():
        for s, nslist in trans[e].iteritems():
            for ns in nslist:
                if s in secret or ns in secret:
                    assert g_public.contains_tran(e,s,ns) == False
                else:
                    assert g_public.contains_tran(e,s,ns) == True
    for s in xrange(g_public.vars['state']['dom']):
        if s not in secret and ns not in secret:
            assert g_public.contains_tran(0,s,s) == True   

def test_trivial_goodEdit():
    a, trans, secret = build_aut0()
    g_edit = op.build_editFSM(a, trans)
    g_public = op.build_publicFSM(a)
    good_edit = op.build_good_editFSM(g_edit, g_public, None)
    assert good_edit == None 
 
def test_goodEdit():
    a, trans, secret = build_aut1()
    g_edit = op.build_editFSM(a, trans)
    g_public = op.build_publicFSM(a)
    util = [[0,0],[1,1],[2,2],[3,3],[4,4],[5,5],[6,6],[0,1],[5,2],[6,3],[7,4],[0,2],[8,3],[0,5],[8,6]]
    assert g_edit.vars['event']['bitnames'] == g_public.vars['event']['bitnames']
    good_edit = op.build_good_editFSM(g_edit, g_public, util)
    old_states = good_edit.bdd.False
    states = good_edit.init
    while old_states != states:
        old_states = states
        new_states = good_edit.trans & states
        new_states = new_states | good_edit.insert_trans & states
        new_states = new_states | good_edit.replace_trans & states
        new_states = good_edit.bdd.quantify(new_states, good_edit.bdd.vars-set(good_edit.vars['nstate']['bitnames']), False)
        new_states = cudd.rename(new_states, good_edit.bdd, good_edit.unprime) 
        states = states | new_states
    u = good_edit.bdd.False
    for pair in util:
        r = bv.var_to_cube(pair[0], g_edit.vars['state'], good_edit.bdd)
        f = bv.var_to_cube(pair[1], g_public.vars['state'], good_edit.bdd)
        u = u | r & f
    assert states & ~u == good_edit.bdd.False
    del old_states, states, new_states, u, r, f

if __name__ == '__main__':
    test_trivial_goodEdit()
