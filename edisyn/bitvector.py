"""Bit-blasting for signed integer arithmetic.

This module translates Boolean formulas that can
contain arithmetic expressions involving signed integers
to bitvector propositional formulas.

Reference
=========

Chapter 6, in particular pp. 158--159 from:
Kroening D. and Strichman O.
Decision Procedures, Springer
"""
import logging
import math
import pdb

ALU_BITWIDTH = 32
logger = logging.getLogger(__name__)

def bitblast_table(table):
    """Return table of variables for bitvectors.

    `table` is a `dict` that maps each variable
    to a `dict` with attributes:

      - "owner": `in ('env', 'sys')`
      - "dom": `tuple([min, max])` where `min, max` are `int`
        used only if "type" is an integer
      - "init" (optional)
    """
    t = dict()
    for var, d in table.iteritems():
        b = dict(d)  # cp other keys
        dom = d['dom']
        assert dom >= 0, dom
        width = dom_to_width(dom)
        b.update(width=width)
        t[var] = b
    _add_bitnames(t)
    return t

def int_to_bitfield(n):
    return [int(digit) for digit in bin(n)[2:]]


def tran_to_boolean(ev, s, ns, vars):
    sexpr = var_to_boolean(s, vars['state'])
    nsexpr = var_to_boolean(ns, vars['nstate'])
    evexpr = var_to_boolean(ev, vars['event'])
    return evexpr + ' & ' + sexpr + ' & ' + nsexpr

def tran_to_cube(ev, s, ns, vars, bdd):
    s = var_to_cube(s, vars['state'], bdd)
    ns = var_to_cube(ns, vars['nstate'], bdd)
    ev = var_to_cube(ev, vars['event'], bdd)
    return s & ev & ns

def var_to_cube(s, vardata, bdd):
    """"s_0 is the left most bit (MSB)"""
    varname = vardata['name']
    #pdb.set_trace()
    sbits = int_to_bitfield(s)
    assert vardata['width'] >= len(sbits), (vardata['width'],len(sbits))
    fill = vardata['width'] - len(sbits)
    for i in xrange(fill):
        #append bits to the left
        sbits.insert(0,0)
    assert vardata['width'] == len(sbits), sbits
    cube_list = dict()
    #bnames = vardata['bitnames']
    #if sbits[0]:
    #    expr = bnames[0] 
    #else: expr = '!'+bnames[0]
    #pdb.set_trace()
    for i in xrange(len(sbits)):
        if sbits[i]:
            cube_list['{s}_{i}'.format(s = varname,i = i)]=True
        else: cube_list['{s}_{i}'.format(s = varname, i = i)]=False
    return bdd.cube(cube_list)

def var_to_boolean(s, vardata):
    """"s_0 is the left most bit (MSB)"""
    varname = vardata['name']
    sbits = int_to_bitfield(s)
    assert vardata['width'] >= len(sbits), (vardata['width'],len(sbits))
    fill = vardata['width'] - len(sbits)
    for i in xrange(fill):
        #append bits to the left
        sbits.insert(0,0)
    assert vardata['width'] == len(sbits), sbits
    bnames = vardata['bitnames']
    if sbits[0]:
        expr = bnames[0] 
    else: expr = '!'+bnames[0]
    #pdb.set_trace()
    for i in xrange(1, len(sbits)):
        if sbits[i]:
            expr1 = " & {name}".format(name = bnames[i]) 
        else: expr1 = " & !{name}".format(name =bnames[i])
        expr += expr1
    return expr

def bit_to_integer(fbits, varbits, bdd):
    r = 0
    for bit in varbits:
        if bdd.cofactor(fbits, dict({bit: True})) == bdd.False:
            r = 2*r 
        else: r = 2*r+1 
    return r


def dom_to_width(dom):
    """Return whether integer variable is signed and its bit width.

    @param dom: the variable's range
    @type dom: `(MIN, MAX)` where `MIN, MAX` are integers
    """
    width = (dom-1).bit_length()
    if width == 0:
        assert dom == 0 
        width = 1
    return width


def _add_bitnames(t):
    """Map each integer to a list of bit variables."""
    # str_to_int not needed, because there
    # are no string variables in Promela
    for var, d in t.iteritems():
        bits = ['{name}_{i}'.format(name=d['name'], i=i)
                    for i in xrange(d['width'])]
        d['bitnames'] = bits


def _check_data_types(t):
    types = {'bool', 'int'}
    for var, d in t.iteritems():
        if d['type'] in types:
            continue
        raise Exception(
            'unknown type: "{dtype}"'.format(dtype=d['type']))


def list_bits(dvars):
    """Return symbol table of bits (comprising bitfields).

    For symbol table definition, see `bitblast_table`.

    @param dvars: symbol table of integer and Boolean vars
    @type dvars: `dict` of `dict`
    @return: symbol table of bits
    @rtype: `dict` of `dict`
    """
    dout = dict()
    c = dvars['bitnames']
    dcp = dict(dvars)  # cp other keys
    for bit in c:
        dbit = dict(dcp)
        dout[bit] = dbit
    return dout


def bitfields_to_ints(bit_state, t):
    """Convert bits to integer for state `dict`.

    @param bit_state: assignment to all bits
    @type bit_state: `dict`
    @type t: `VariablesTable`
    """
    int_state = dict()
    for flatname, d in t.iteritems():
        if d['type'] == 'bool':
            int_state[flatname] = bit_state[flatname]
            continue
        # this is an integer var
        bitnames = d['bitnames']
        bitvalues = [bit_state[b] for b in bitnames]
        _append_sign_bit(bitvalues, flatname, d)
        int_state[flatname] = twos_complement_to_int(bitvalues)
    return int_state


